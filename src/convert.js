const pdf2html = require('pdf2html');
const path = require('path');
const fs = require('fs');
const pdf = require('html-pdf');
const { shell } = require('electron');
//const progress = require('progress-stream');
const pdfDocument = require('pdfkit');
var pdf2img = require('pdf2img');

module.exports = {
  disabledElFrom: 0,
  disabledElTo: 0,
  //button class sample pdfcon-pdf/docx/html/text-from/to
  //TODO: Make nice way of not choosing same file types,
  //add different colors for types
  onClickConvertFrom: function(e){
    if (e.currentTarget.textContent == convertTo) {
      alert('Please choose different file types.');
      return;
    }
    convertFrom = e.currentTarget.textContent;
    $('#pdfcon-from').text(convertFrom);
    $('#pdfcon-from').css('background', '#339933');
    console.log('Choosed from: ', convertFrom);
  },
  //button class sample pdfcon-pdf/docx/html/text-from/to
  onClickConvertTo: function(e){
    if (e.currentTarget.textContent == convertFrom) {
      alert('Please choose different file types.');
      return;
    }
    convertTo = e.currentTarget.textContent;
    $('#pdfcon-to').text(convertTo);
    $('#pdfcon-to').css('background', '#339933');
    console.log('Choosed to: ', convertTo);
  },
  convertPDFtoHTML: function(){
    pdf2html.html(sourcePDF , (err, html) => {
      if (err) {
        console.error('Conversion error: ' + err)
      } else {
        let basename = path.basename(sourcePDF);
        let htmlFilePath = basename.replace(path.extname(basename), '.html');
        htmlFilePath = outputFolder + path.sep + htmlFilePath;
        //TODO: Use other convert lib // fs.createWriteStream(htmlFilePath);
        fs.writeFile(htmlFilePath, html, (err) => {
          if (err) {
            //TODO: Properly handle throw
            console.log(err);
            throw err;
          } else{
            //TODO: Make progress bar work as it lasts too long
            console.log('New HTML file has been saved!');
            if (shell.openItem(htmlFilePath)) {
              alert('The "' + convertFrom + '" file to "' + convertTo + '" file conversion completed.');
            }
          }
        });
      }
    });
  },
  convertHTMLtoPDF: function(t){
    let html = fs.readFileSync(sourcePDF, 'utf8');
    let options = { type: 'pdf', format: 'A4' };
    let basename = path.basename(sourcePDF);
    let pdfFilePath = basename.replace(path.extname(basename), '.pdf');
    pdfFilePath = outputFolder + path.sep + pdfFilePath;
    pdf.create(html, options).toFile(pdfFilePath, function(err, res) {
      if (err) {
        console.log(err);
        throw err;
      } else {
        console.log('New ' + res.filename + 'PDF file has been saved!');
        if (shell.openItem(pdfFilePath)) {
          alert('The "' + convertFrom + '" file to "' + convertTo + '" file conversion completed.');
        }
      }
    });
  },
  convertPDFtoIMG: function(t){
    let basename = path.basename(sourcePDF);
    let newPngFilePath = outputFolder + path.sep + basename.replace(path.extname(basename), '');
    pdf2img.setOptions({
      type: 'png',      // png or jpg, default jpg
      size: 1024,       // default 1024
      density: 600,     // default 600
      outputdir: outputFolder, // if null then it will be as file name
      outputname: basename, // if null it will be as input name
      page: null   // convert selected page, if null will convert all pages
    });
    pdf2img.convert(sourcePDF, function(err, info) {
      if (err) {
        console.log(err);
        throw err;
      } else {
        console.log('New ' + info.message.length + ' ' + newPngFilePath + 'IMAGE files have been saved!');
        if (shell.openItem(info.message[0].path)) {
          alert('The "' + convertFrom + '" file to "' + convertTo + '" file conversion completed.');
        }
      }
    });
  },
  convertIMGtoPDF: function(t){
    let basename = path.basename(sourcePDF);
    let newPDFFilePath = basename.replace(path.extname(basename), '.pdf');
    newPDFFilePath = outputFolder + path.sep + newPDFFilePath;
    console.log("Generated file path is:", newPDFFilePath);
    const doc = new pdfDocument();
    doc.pipe(fs.createWriteStream(newPDFFilePath));
    doc.image(sourcePDF, {
      fit: [250, 300], //TODO: Fix size, now is too small
        align: 'center',
        valign: 'center'
    });
    doc.end();
  },
  runConvertionbyType: function(t){
    //TODO: Add Loading as it completes very slowly
    switch (t){
      //PDF conversion
      case 'PDFHTML':
          Convert.convertPDFtoHTML();
          //done
        break;
      case 'PDFDOCX':
          Convert.convertPDFtoDOCX();
        break;
      case 'PDFTEXT':
          Convert.convertPDFTEXT();
        break;
      case 'PDFIMG':
          Convert.convertPDFtoIMG();
          //done
        break;
      //HTML conversion
      case 'HTMLPDF':
        Convert.convertHTMLtoPDF();
          //done
        break;
      case 'HTMLIMG':
        Convert.convertHTMLtoIMG();
        break;
      //IMG conversion
      case 'IMGPDF':
          Convert.convertIMGtoPDF();
          //done?
        break;
      default:
        console.log("The feature isn't implemented yet.");
        break;
    }
  },
  onClickConvertGenerate: function(){
    if (0 == outputFolder) {
      alert('Please choose the source file.');
       return;
    }
    if (! fs.existsSync(outputFolder)) {
      fs.mkdirSync(outputFolder);
    }
    var t = convertFrom + convertTo;
    Convert.runConvertionbyType(t);
  }
};
